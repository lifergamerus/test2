package main

import (
	_ "awesomeProject/docs"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/files"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net/http"
	"time"
)

var db *gorm.DB

type Task struct {
	ID        uint      `gorm:"primarykey"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	Title     string
	// Добавьте другие поля вашей модели
}

// ErrorResponse представляет собой структуру для описания ошибки.
type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func main() {
	// Строка подключения к PostgreSQL
	dsn := "host=localhost user=postgres password=MadMan3002 dbname=somegtxate port=5432 sslmode=disable"

	// Открываем соединение с базой данных
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	// Автомиграция модели Task
	db.AutoMigrate(&Task{})

	// Создаем экземпляр Gin-роутера
	r := gin.Default()

	// Маршруты для вашего CRUD API
	r.POST("/tasks", createTask)
	r.GET("/tasks", getTasks)
	r.GET("/tasks/:id", getTask)
	r.PUT("/tasks/:id", updateTask)
	r.DELETE("/tasks/:id", deleteTask)

	// Swagger роут
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Запускаем сервер на порту 8080
	fmt.Println("Сервер запущен на порту 8080...")
	if err := http.ListenAndServe(":8080", r); err != nil {
		fmt.Println(err)
	}
}

// @Summary Создать задачу
// @Description Создает новую задачу
// @Tags Задачи
// @Accept  json
// @Produce  json
// @Param input body Task true "Данные для создания задачи"
// @Success 201 {object} Task
// @Failure 400 {object} ErrorResponse
// @Router /tasks [post]
func createTask(c *gin.Context) {
	var task Task
	if err := c.ShouldBindJSON(&task); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.Create(&task)

	c.JSON(http.StatusCreated, task)
}

// @Summary Получает задачу
// @Description Получает задачи
// @Tags Задачи
// @Accept  json
// @Produce  json
// @Success 201 {object} []Task
// @Failure 400 {object} ErrorResponse
// @Router /tasks [get]
// Функция для получения всех задач
func getTasks(c *gin.Context) {
	var tasks []Task
	db.Find(&tasks)

	c.JSON(http.StatusOK, tasks)
}

// @Summary Получить задачу по ID
// @Description Получает задачу по указанному идентификатору
// @Tags Задачи
// @Accept  json
// @Produce  json
// @Param id path int true "ID задачи" Format(int64)
// @Success 200 {object} Task
// @Failure 404 {object} ErrorResponse
// @Router /tasks/{id} [get]
func getTask(c *gin.Context) {
	var task Task
	id := c.Param("id")
	db.First(&task, id)

	if task.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"error": "Задача не найдена"})
		return
	}

	c.JSON(http.StatusOK, task)
}

// Функция для обновления задачи по ID
func updateTask(c *gin.Context) {
	var task Task
	id := c.Param("id")
	db.First(&task, id)

	if task.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"error": "Задача не найдена"})
		return
	}

	if err := c.ShouldBindJSON(&task); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.Save(&task)

	c.JSON(http.StatusOK, task)
}

// Функция для удаления задачи по ID
func deleteTask(c *gin.Context) {
	var task Task
	id := c.Param("id")
	db.First(&task, id)

	if task.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"error": "Задача не найдена"})
		return
	}

	db.Delete(&task)

	c.JSON(http.StatusOK, gin.H{"message": "Задача удалена"})
}
